package other;

public class Swap {
    public static void main(String[] args) {
        Thread
        //值交换的三种方式
        int a=7;
        int b=11;
        //1、和取差
        int sum=a+b;
        a=sum-a;
        b=sum-b;
        //2、临时空间
        int tem=a;
        a=b;
        b=tem;

        //3、亦或  切记两个相同的数不可以通过此方法交换，否则都为0
        a=a^b;
        b=a^b;
        a=a^b;

    }
}
